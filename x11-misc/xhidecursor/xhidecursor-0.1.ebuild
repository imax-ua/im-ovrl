# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="xhidecursor is a minimal X-application which hides the cursor on key-press and unhides the cursor on mouse-movement. The two main advantages compared to other popular alternatives like xbanish"
HOMEPAGE="https://github.com/astier/xhidecursor"

inherit git-r3
EGIT_REPO_URI="https://github.com/astier/${PN}.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="
	x11-libs/libXi
	x11-libs/libXfixes
	"
RDEPEND="${DEPEND}"

src_prepare() {
    default
	}

src_compile() {
    :
}

